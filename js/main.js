$(function() {
    contactButton();
    navIcon();
})


function contactButton() {
    $('.contact-button').on('click', function(e) {
        let $ul = $(this).parents('ul');
        $ul.toggleClass('show');
        e.preventDefault();
    })
}

// Function for manipulating navbar icon
function navIcon() {
    $('#nav-icon').click(function() {
        $(this).toggleClass('open');
        displayHiddenList();
    })
}

// Function for display hidden list on navbar
function displayHiddenList() {
    let $hl = $('.hidden-list');
    $hl.toggleClass('hidden-list-show');
}